package org.example.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class SelectDropDownListPage extends BasePage {

    private final By singleDropDown = By.id("select-demo");
    private final By multiSelectList = By.id("multi-select");

    public void selectFromDropDown(String day){
        Select select = new Select(find(singleDropDown));
        select.selectByVisibleText(day);
    }

    public void selectMultipleFromMultiList(List<String> optionTexts){
        Select select = new Select(find(multiSelectList));
        optionTexts.forEach(stateName -> select.selectByVisibleText(stateName));
    }


}
