package org.example.pages;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class JavascriptAlertsPage extends BasePage {

    private final By allClickMeButtons = By.xpath("//button[text()='Click Me']");

    public void clickThePromptBox(){
        click(allClickMeButtons, 2);
    }

    public void inputTextAndPressOk(String input){
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        Alert alert = wait.until(ExpectedConditions.alertIsPresent());
        //Alert alert = driver.switchTo().alert();
        alert.sendKeys(input);
        alert.accept();
    }

}
