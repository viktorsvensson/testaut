package org.example.pages;

import org.openqa.selenium.By;

public class SimpleFormDemoPage extends BasePage {

    private final By singleInputField = By.id("user-message");
    private final By getCHeckedValueButton = By.id("showInput");

    private final By displayedMessage = By.id("message");

    public void typeIntoSingleInputField(String input){
        sendKeys(singleInputField, input);
    }

    public void clickGetCHeckedValueButton(){
        click(getCHeckedValueButton);
    }
;
    public String getDisplayedMessageText(){
        return getText(displayedMessage);
    }

}
