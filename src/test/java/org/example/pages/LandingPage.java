package org.example.pages;

import org.openqa.selenium.By;
public class LandingPage extends BasePage {

    private final By simpleFormDemoLink = By.linkText("Simple Form Demo");
    private final By dropDownListLink = By.linkText("Select Dropdown List");
    private final By JsAlertsLink = By.linkText("Javascript Alerts");
    private final By downloadProgressLink = By.linkText("Bootstrap Progress bar");

    public SimpleFormDemoPage followLinkToSimpleFormDemo(){
        click(simpleFormDemoLink);
        return new SimpleFormDemoPage();

    }

    public SelectDropDownListPage followLinkSelectDropDownListPage(){
        click(dropDownListLink);
        return new SelectDropDownListPage();
    }

    public JavascriptAlertsPage followLinkToJavascriptALerts(){
        click(JsAlertsLink);
        return new JavascriptAlertsPage();
    }

    public DownloadProgressPage followLinkToDownloadProgressPage(){
        click(downloadProgressLink);
        return new DownloadProgressPage();
    }
}
