package org.example.selenium;

import org.example.pages.SelectDropDownListPage;
import org.testng.annotations.Test;

import java.util.List;

public class SelectDropDownListTest extends BaseTest {

    private SelectDropDownListPage selectDropDownListPage;

    @Test
    public void selectFromSingleDropDown(){
        selectDropDownListPage = landingPage.followLinkSelectDropDownListPage();
        selectDropDownListPage.selectFromDropDown("Tuesday");
    }

    @Test
    public void selectMultipleOptions(){
        selectDropDownListPage = landingPage.followLinkSelectDropDownListPage();
        selectDropDownListPage
                .selectMultipleFromMultiList(List.of("Texas", "Ohio", "Florida"));
    }

}
