package org.example.selenium;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.example.pages.LandingPage;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

public class BaseTest {

    private WebDriver driver;
    private final static String baseUrl = "https://lambdatest.com/selenium-playground";
    protected static LandingPage landingPage;

    @BeforeClass
    public void setup(){
        WebDriverManager.chromedriver().setup();
    }

    @BeforeMethod
    public void init(){
        ChromeOptions option = new ChromeOptions();
        option.addArguments("--remote-allow-origins=*");
        driver = new ChromeDriver(option);
        // Only for multiple monitors
        driver.manage().window().setPosition(new Point(2000, 0));
        driver.manage().window().maximize();
        driver.get(baseUrl);
        landingPage = new LandingPage();
        landingPage.setDriver(driver);
    }

}
