package org.example.selenium;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.locators.RelativeLocator;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class DemoTests {

    private WebDriver driver;

    private final static String baseUrl = "https://lambdatest.com/selenium-playground";

    @BeforeClass
    public void setup(){
        WebDriverManager.chromedriver().setup();
    }

    @BeforeMethod
    public void init(){
        ChromeOptions option = new ChromeOptions();
        option.addArguments("--remote-allow-origins=*");
        driver = new ChromeDriver(option);
        // Only for multiple monitors
        driver.manage().window().setPosition(new Point(2000, 0));

        driver.manage().window().maximize();
    }

    @Test
    public void demo() throws InterruptedException {
        driver.get("https://learnpoint.se");
        driver.get("https://google.se");
        driver.navigate().back();
    }

    @Test
    public void simpleFormDemo_enterInput_shouldDisplayInput() throws InterruptedException {
        driver.get("https://lambdatest.com/selenium-playground");
        Thread.sleep(2000);
        driver.findElement(By.linkText("Simple Form Demo")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("user-message")).sendKeys("Hej");
        Thread.sleep(2000);
        driver.findElement(By.id("showInput")).click();
        Thread.sleep(2000);
        String message = driver.findElement(By.id("message")).getText();

        Assert.assertEquals(message, "Hej");

    }

    @Test
    public void advancedButtonInteraction(){
        driver.get("https://demoqa.com/buttons");
        WebElement doubleCLickButton = driver.findElement(By.id("doubleClickBtn"));
        WebElement rightClickButton = driver.findElement(By.id("rightClickBtn"));

        Actions actions = new Actions(driver);
        actions.doubleClick(doubleCLickButton).perform();
        actions.contextClick(rightClickButton).perform();

    }

    @Test
    public void dynamicButtonCLick(){
        driver.get("https://demoqa.com/buttons");

        By dynamicButton = RelativeLocator.with(By.tagName("button")).near(By.id("rightClickBtn"));
        driver.findElement(dynamicButton).click();
    }

    @Test
    public void radioButtons(){
        driver.get(baseUrl + "/radiobutton-demo");
        // xpath syntax "//tagName[@attribute='attributeValue' and ...]
        driver.findElement(By.xpath("//input[@name='optradio' and @value='Male']")).click();
        driver.findElement(By.xpath("//button[text()='Get Checked value']")).click();
    }

    @AfterMethod
    public void tearDown(){
        //driver.quit();
    }




}
