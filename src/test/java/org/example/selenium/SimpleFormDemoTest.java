package org.example.selenium;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.example.pages.LandingPage;
import org.example.pages.SimpleFormDemoPage;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class SimpleFormDemoTest extends BaseTest {

    @Test
    public void simpleFormDemo_whenInputText_shoulDisplayText(){
        // Given
        String testMessage = "test";

        // When
        SimpleFormDemoPage simpleFormDemoPage = landingPage.followLinkToSimpleFormDemo();
        simpleFormDemoPage.typeIntoSingleInputField(testMessage);
        simpleFormDemoPage.clickGetCHeckedValueButton();
        String result = simpleFormDemoPage.getDisplayedMessageText();

        // Then
        Assert.assertEquals(result, testMessage);

    }
}