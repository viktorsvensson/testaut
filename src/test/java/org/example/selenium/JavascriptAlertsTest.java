package org.example.selenium;

import org.example.pages.JavascriptAlertsPage;
import org.testng.annotations.Test;

public class JavascriptAlertsTest extends BaseTest {

    private JavascriptAlertsPage javascriptAlertsPage;

    @Test
    public void clickAndInputAlertBox(){
        javascriptAlertsPage = landingPage.followLinkToJavascriptALerts();
        javascriptAlertsPage.clickThePromptBox();
        javascriptAlertsPage.inputTextAndPressOk("Viktor");
    }

}
