package org.example.selenium;

import org.example.pages.DownloadProgressPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class DownloadProgressTest extends BaseTest {

    private DownloadProgressPage downloadProgressPage;

    @Test
    public void waitAndCheckDisplayedSuccessMessage(){
        downloadProgressPage = landingPage.followLinkToDownloadProgressPage();
        downloadProgressPage.clickDownloadButton();
        String result = downloadProgressPage.waitAndReturnSuccessMessage();
        Assert.assertEquals(result, "Dowload completed!");
    }

}
