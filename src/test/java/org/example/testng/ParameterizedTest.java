package org.example.testng;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class ParameterizedTest {

    @Test(dataProviderClass = TestData.class , dataProvider = "argsSupplier", timeOut = 2000)
    public void dogsTest(String name, int age) throws InterruptedException {
        System.out.println("Name: " + name);
        if(name.equals("Fido")){
            Thread.sleep(10000);
        }
        System.out.println("Age: " + age);
    }

    @DataProvider
    public Object[][] argsSupplier(){
        return new Object[][] {
                {"Fido", 12},
                {"Maja", 7},
                {"Scilla", 8}
        };
    }

}
