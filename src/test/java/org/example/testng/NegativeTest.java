package org.example.testng;

import org.testng.annotations.Test;

public class NegativeTest {

    @Test(expectedExceptions = ArithmeticException.class,
        expectedExceptionsMessageRegExp = "/ by zero"
    )
    public void divide_byZero_shouldTHrow(){
        int sum = 5 / 0;
    }

}
