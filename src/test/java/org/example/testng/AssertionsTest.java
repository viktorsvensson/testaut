package org.example.testng;

import org.testng.Assert;
import org.testng.annotations.Test;

public class AssertionsTest {

    @Test
    public void assert_whenSomethinsIsSomeway_shouldDOSomething(){
        // Given
        int a = 5;
        int b = 2;

        // When
        int sum = add(a, b);

        //Then
        Assert.assertEquals(sum, 7);
    }

    private int add(int a, int b) {
        return a + b;
    }

}
