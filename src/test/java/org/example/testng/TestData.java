package org.example.testng;

import org.testng.annotations.DataProvider;

public class TestData {

    @DataProvider(parallel = true)
    public Object[][] argsSupplier(){
        return new Object[][] {
                {"Fido", 120},
                {"Maja", 70},
                {"Scilla", 80}
        };
    }

}
