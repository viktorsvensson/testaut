package org.example.testng;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class CycleTest {

    @BeforeClass
    public void beforeClass(){
        System.out.println("-- CycleTest_beforeClass");
    }

    @BeforeMethod
    public void beforeMethod(){
        System.out.println("--- CycleTest_beforeMethod");
    }

    @Test
    public void testMethod1(){
        System.out.println("---- CycleTest_testMethod1");
    }

    @Test(groups = "smoke")
    public void atestMethod2(){
        System.out.println("---- CycleTest_testMethod2");
    }

}
