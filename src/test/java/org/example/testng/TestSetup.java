package org.example.testng;

import org.testng.annotations.BeforeTest;

public class TestSetup {

    @BeforeTest(alwaysRun = true)
    public void beforeTest(){
        System.out.println("- beforeTest");
    }

}
