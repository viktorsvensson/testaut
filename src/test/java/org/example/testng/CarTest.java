package org.example.testng;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class CarTest {

    @BeforeClass
    public void beforeClass(){
        System.out.println("-- beforeClass");
    }

    @BeforeMethod
    public void beforeMethod(){
        System.out.println("--- CarTest_beforeMethod");
    }

    @Test(dependsOnGroups = {"smoke"})
    //@Test(priority = 2)
    public void testMethod1(){
        System.out.println("---- CarTest_testMethod1");
    }

    @Test(priority = 1, groups = {"regression", "smoke", "database"})
    public void testMethod2() throws InterruptedException {
        Thread.sleep(10000);
        System.out.println("---- CarTest_testMethod2");
    }

}
