package org.example.testng;

import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class StreamTest {

    @Test
    public void stream(){

        // FIltrera bort alla ojåmna tal från en lista
        // klassiskt
        List<Integer> numbers = List.of(1, 6, 3, 6, 326, 34, 54);

        List<Integer> filteredList = new ArrayList<>();
        for(int i = 0; i < numbers.size(); i++){
            if(numbers.get(i) % 2 == 0){
                filteredList.add(numbers.get(i));
            }
        }
        System.out.println(filteredList);

        List<Integer> filteredList2 = numbers.stream()
                .filter(x -> x % 2 == 0)
                .toList();
        System.out.println(filteredList2);

    }

}
